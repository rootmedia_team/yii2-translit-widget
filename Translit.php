<?php

namespace rootmedia\translit;

use Yii;
use rootmedia\translit\Assets;
use yii\base\Widget;
use yii\helpers\Json;
use yii\web\JsExpression;

class Translit extends Widget
{
    public $fields = [];
    public $options = [];

    public function init()
    {
//        if(!isset($this->options['reg'])){
//            $this->options['reg'] = '" "="-",\(="",\)=""';
//        }
//        $this->options['eventType'] = 'keyup';
        parent::init();
    }

    public function run()
    {
        Assets::register($this->getView());
        if(is_array($this->fields)){
            foreach ($this->fields as $els) {
                if(isset($els['out']) && isset($els['in'])){
                    $this->options['destination'] = $els['out'];
                    $this->view->registerJs("$('#{$els['in']}').syncTranslit(".Json::encode($this->options).");");
                    $this->view->registerJs("$('#{$els['out']}').syncTranslit(".Json::encode($this->options).");");
                }
            }
        }
    }
}