<?php
namespace rootmedia\translit;

use yii\web\AssetBundle;

class Assets extends AssetBundle
{
    public $css = [];

    public $js = [
//        'js/jquery.liTranslit.js',
        'js/jquery.synctranslit.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];

    public function init()
    {
        $this->sourcePath = __DIR__ . '/assets';
        parent::init();
    }
}